package com.example.demo;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.entity.Employee;
import com.example.demo.repository.EmployeeRepository;


@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
class CrudmysqlApplicationTests {

	
	
	
	@BeforeEach
	public void before()
	{
		System.out.println("Test cases to be initiated");
		
	}
	
	
	@Autowired
	EmployeeRepository eRepo;
	 
	@Test
	@Order(1)
	public void testCreate() {
		
		Employee e = new Employee();
		e.setId(49L);
		e.setName("jai");
		e.setSalary(18000L);
		eRepo.save(e);
		
		assertNotNull(eRepo.findById(48L).get());
		
	}
	
	
	@Test
	@Order(2)
	public void testReadAll()
	{
		List<Employee> l = eRepo.findAll();
		assertThat(l).size().isGreaterThan(0);
	}
	
	
	@Test
	@Order(3)
	public void testSingleRead()
	{
		Employee e =eRepo.findById(8L).get();
		assertEquals(14000L, e.getSalary());
	}

	
	@Test
	@Order(4)
	public void testUpdate()
	{
		Employee e = eRepo.findById(9L).get();
		e.setName("karthi");
		eRepo.save(e);
		assertNotEquals("vimal", eRepo.findById(9L).get().getName());
	}
	
	@Disabled
	@Test
	@Order(5)
	public void testDelete()
	{
		eRepo.deleteById(47L);
		assertThat(eRepo.existsById(47L)).isFalse();	
	}
	
	@AfterEach
	 public void after() {
         System.out.println("Test cases are successfully initiated");
		
	}
	


	
	
	
		
}
