package com.example.demo.Controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Employee;
import com.example.demo.repository.EmployeeRepository;

@RestController
public class EmployeeController {
	
	@Autowired
	EmployeeRepository EmployeeRepo;
	
	@GetMapping("/employee")
	public ResponseEntity<?> getAllEmployee()
	{
		List<Employee> Emp = EmployeeRepo.findAll();
		if (Emp.size() > 0)
		{
			return new ResponseEntity<List<Employee>> (Emp, HttpStatus.OK);
		}
		else
		{
			return new ResponseEntity<>("No emp available", HttpStatus.NOT_FOUND);
		}
	}
	
	 @PostMapping("/employee")
			public ResponseEntity<?> createEmp(@RequestBody Employee emp)
			{
				try
				{
					emp.setCreatedAt(new Date(System.currentTimeMillis()));
					EmployeeRepo.save(emp);
					return new ResponseEntity<Employee>(emp, HttpStatus.OK);
				}
				catch (Exception e)
				{
					return new ResponseEntity<>("e.getMessage", HttpStatus.INTERNAL_SERVER_ERROR); 
				}
			}
	 
	 @PutMapping ("/employee/{id}")
	    public ResponseEntity<?> updatedById(@PathVariable("id") Long id, @RequestBody Employee emp)
	    {
	    	Optional<Employee> emplOptional = EmployeeRepo.findById(id);
	    	if(emplOptional.isPresent()) {
	    		Employee empToSave = emplOptional.get();
	    		empToSave.setName(emp.getName()!=null ? emp.getName() : empToSave.getName());
	    		empToSave.setSalary(emp.getSalary()!=null ? emp.getSalary() : empToSave.getSalary());
	    		empToSave.setUpdatedAt(new Date(System.currentTimeMillis()));
	    		EmployeeRepo.save(empToSave);
	    		return new ResponseEntity<>(empToSave, HttpStatus.OK);
	    	}
	    	else {
	    	return new ResponseEntity<>("Todo not found with id"+id, HttpStatus.NOT_FOUND);
	    }
	    }
	 
	 @DeleteMapping("/employee/{id}")
	 public ResponseEntity<?> deleteById(@PathVariable("id") Long id) {
			try{
	            EmployeeRepo.deleteById(id);
	            return new ResponseEntity<>("Successfully deleted todo with id "+id, HttpStatus.OK);
	        }
	        catch (Exception e)
	        {
	            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
	        }	
		}
}


