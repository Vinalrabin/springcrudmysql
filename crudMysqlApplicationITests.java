package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;

import com.example.demo.entity.Employee;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)

public class crudMysqlApplicationITests {
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	 private String getRootUrl() {
		 
         return "http://localhost:" + port;
     }

     @Test
     public void contextLoads() {
	
     }
		@Test
		@Order(1)
     public void testgetall()
     {
    	HttpHeaders headers = new HttpHeaders();
    	HttpEntity<String> entity = new HttpEntity<String> (null, headers);
    	ResponseEntity<String> response = restTemplate.exchange(getRootUrl() +"/employee", HttpMethod.GET, entity, String.class);
    	assertNotNull(response.getBody());
     }
		
		@Test
		@Order(2)
		public void testgetById()
		{
			Employee emp = restTemplate.getForObject(getRootUrl() + "/employee/34", Employee.class);
			System.out.println(emp.getName());
			assertNotNull(emp);
			
		}
		
		 @Test
			@Order(4)
		    public void testCreate() {
		        Employee employee = new Employee();
		        employee.setId(44L);
		        employee.setName("server");
		        employee.setSalary(45000L);
		        ResponseEntity<Employee> postResponse = restTemplate.postForEntity(getRootUrl() + "/employee", employee, Employee.class);
		        assertNotNull(postResponse);
		        assertNotNull(postResponse.getBody());
     
}
		 
		 @Test
			@Order(3)
		    public void testUpdate() {
		        Long id = 36L;
		        Employee emp = restTemplate.getForObject(getRootUrl() + "/employee/" + id, Employee.class);
		        emp.setName("admin1");
		        emp.setSalary(100000L);
		        restTemplate.put(getRootUrl() + "/employee/" + id, emp);
		        Employee updatedEmployee = restTemplate.getForObject(getRootUrl() + "/employee/" + id, Employee.class);
		        assertNotNull(updatedEmployee);
		    }

		 @Test
			@Order(5)
		    public void testDeleteEmployee() {
		         Long id = 40L;
		         Employee employee = restTemplate.getForObject(getRootUrl() + "/employee/" + id, Employee.class);
		         assertNotNull(employee);
		         restTemplate.delete(getRootUrl() + "/employee/" + id);
		         try {
		              employee = restTemplate.getForObject(getRootUrl() + "/employee/" + id, Employee.class);
		         } catch (final HttpClientErrorException e) {
		              assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);

}
		 }
}